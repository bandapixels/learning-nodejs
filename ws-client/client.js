const WebSocketClient = require('websocket').client;

const client = new WebSocketClient();

client.on('connectFailed', function(error) {
  console.log('Connect Error: ' + error.toString());
  console.log('Retry in 5 seconds');
  setTimeout(() => client.connect('ws://localhost:3000/'), 5000);
});

client.on('connect', function(connection) {
  console.log('Connection established!');

  connection.on('error', function(error) {
    console.log("Connection error: " + error.toString());
  });

  connection.on('close', function() {
    console.log('Connection closed!');
  });

  connection.on('message', function(message) {
    console.log('New message from server:');
    console.log(message);
  });
});

client.connect('wss://localhost:3000/');
