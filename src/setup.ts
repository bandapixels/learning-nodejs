import { join } from 'node:path';
import { access, mkdir } from 'node:fs/promises';
import { setup } from './dataservice';

export const prepare = async () => {
  const ASSETS_DIR_NAME = 'images';
  const ASSETS_DIR_PATH = join('.', ASSETS_DIR_NAME);

  const hasAssetsDir = await access(ASSETS_DIR_PATH).then(() => true, () => false);

  if (!hasAssetsDir) {
    await mkdir(ASSETS_DIR_PATH);
  }

  await setup();

  return ASSETS_DIR_PATH;
}
