import bcrypt from 'bcrypt';
import * as db from '../models/index.js';

export const setup = async () => {
  await db.sequelize.authenticate();
};

export const getUserById = async (id) => {
  return (await db.User.findOne({
    where: { id },
    include: db.GoogleProfile
  }))?.toJSON();
}

export const addUser = async (user) => {
  const passwordHash = user.password ? await bcrypt.hash(user.password, 11) : null;
  await db.User.create({ username: user.username, password: passwordHash, isAdmin: false });
};

export const authenticate = async (username, password) => {
  const user = (await db.User.findOne({ where: { username }, include: db.GoogleProfile}))?.toJSON();

  if (!user) {
    return false;
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return false;
  }

  return user;
};

export const findUserByProfile = async (profile) => {
  return (await db.GoogleProfile.findOne({ where: { email: profile.email }, include: db.User }))?.toJSON();
}
export const createNewUserByProfile = async (profile) => {
  return (await db.User.create({
    username: profile.email,
    GoogleProfile: profile,
  }, {
    include: [db.GoogleProfile],
  }))?.toJSON();
}
