import * as winston from 'winston';
import * as morgan from 'morgan';

export const logger = winston.createLogger({
  level: process.env.LOG_LEVEL ?  process.env.LOG_LEVEL : process.env.NODE_ENV === 'production' ? 'error' : 'debug',
  transports: [
    new winston.transports.Console(),
  ]
});

export const httpLogger = morgan('tiny');
