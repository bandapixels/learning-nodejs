import * as passport from 'passport';
import * as passportSession from 'passport-session';
import { localStrategy } from 'src/auth/strategies/local';
import { googleStrategy } from 'src/auth/strategies/google';
import { getUserById } from '../dataservice';

export const setupAuth = () => {
  passport.use(passportSession);

  passport.use(localStrategy);

  passport.use(googleStrategy);

  passport.serializeUser((user, cb) => {
    return cb(null, {id: user.id});
  });

  passport.deserializeUser(async (user, cb) => {
    const foundUser = await getUserById(user.id);
    if (!foundUser) {
      return cb(null, false);
    }
    const { password, ...safeUserData } = foundUser;
    return cb(null, safeUserData);
  });
}
