import * as PassportLocal from 'passport-local';
import { authenticate } from 'src/dataservice';

export const localStrategy = new PassportLocal(async (username, password, next) => {
  const user = await authenticate(username, password);
  if (user) {
    return next(null, user);
  }

  next(null, false);
})
