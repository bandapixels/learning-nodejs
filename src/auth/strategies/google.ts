import * as GoogleStrategy from 'passport-google-oauth20';
import { createNewUserByProfile, findUserByProfile } from 'src/dataservice';

export const googleStrategy = new GoogleStrategy.Strategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: "http://localhost:3300/auth/google/callback"
  },
  async (accessToken, refreshToken, profile, cb) => {
    profile = profile._json;
    try {
      const user = await findUserByProfile(profile);
      if (user) {
        console.log('Found existing user:');
        console.log({ user });
        return cb(null, user);
      }
      console.log('NOT found existing user');

      const newUser = await createNewUserByProfile(profile);

      console.log('CREATED NEW USER:');
      console.log({ newUser });

      cb(null, newUser);
    } catch (e) {
      cb(e);
    }
  }
);
