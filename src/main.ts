import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import { AppModule } from './app.module';
import { httpLogger, logger } from 'src/logger';
import * as session from 'express-session';
import Redis from 'ioredis';
import * as ConnectRedis from 'connect-redis';
import { setupAuth } from 'src/auth/authentication';
import * as passport from 'passport';
import { authRouter } from 'src/routes/auth';
import { authMiddleware } from 'src/auth/authorization';
import { errorHandlingMiddleware } from 'src/errors';
import { imageRouter } from 'src/routes/image';
import { addWebsocketServer } from 'src/websocket';
import { ConfigService } from '@nestjs/config';

const RedisStore = ConnectRedis(session);
const sessionRedisClient = new Redis();

async function bootstrap() {
  const port = process.env.PORT;

  const nestApplication = await NestFactory.create(AppModule, { logger: ['error', 'warn'] });
  const configService = nestApplication.get(ConfigService);

  const app = nestApplication.getHttpAdapter();

  // @ts-ignore
  app.set('view engine', 'hbs');

  app.use(httpLogger);

  app.use(express.json());

  app.use(session({
    store: new RedisStore({ client: sessionRedisClient }),
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  }));

  setupAuth();

  app.use(passport.authenticate('session'));

  app.use(authRouter);

  // app.use(authMiddleware);

  app.use('/image', imageRouter(configService.get<string>('ASSETS_DIR_PATH'), logger));

  app.use(express.static('swagger'));

  app.use(errorHandlingMiddleware);

  const server = await nestApplication.listen(port);

  addWebsocketServer(server);
}
bootstrap();
