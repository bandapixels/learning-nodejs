import { server as WebsocketServer } from 'websocket/lib/websocket.js';
import { logger } from './logger';
import { onImageAdd, onImageRemove } from './notifications';


export const addWebsocketServer = (httpServer) => {
  const wsServer = new WebsocketServer({ httpServer, autoAcceptConnections: true });

  wsServer.on('connect', () => {
    logger.debug(`Websocket client connected to process ${process.pid}`);
  });

  onImageAdd((channel, payload) => wsServer.broadcast(`Image was added: ${payload}`));
  onImageRemove((channel, payload) => wsServer.broadcast(`Image was removed: ${payload}`));
}
