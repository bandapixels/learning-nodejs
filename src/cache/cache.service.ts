import { Injectable } from '@nestjs/common';
import Redis from 'ioredis';

@Injectable()
export class CacheService {
  private readonly redisClient = new Redis();

  async getFile(filename: string): Promise<Buffer> {
    return this.redisClient.getBuffer(`file:${filename}`);
  }

  appendFileData(key: string, chunk: Buffer): void {
    this.redisClient.append(key, chunk);
  }

  setKeyExpiration(key: string, expTime: number): void {
    this.redisClient.expire(key, expTime);
  }

}
