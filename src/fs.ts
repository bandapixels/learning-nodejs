import { randomUUID } from 'node:crypto';
import { extension } from 'mime-types';
import { open, readdir, unlink, stat } from 'node:fs/promises';
import path from 'node:path';

export const createFile = async (req, ASSETS_DIR_PATH) => {
  const fileName = `${randomUUID()}.${extension(req.headers['content-type'])}`;
  const file = await open(path.join(ASSETS_DIR_PATH, fileName), 'a');
  req.pipe(file.createWriteStream());

  return fileName;
}

export const listFiles = async (ASSETS_DIR_PATH) => {
  return await readdir(ASSETS_DIR_PATH);
}

export const getFileReadStream = async (ASSETS_DIR_PATH, imageName) => {
  return (await open(path.join(ASSETS_DIR_PATH, imageName), 'r')).createReadStream();
}

export const getFileSize = async (ASSETS_DIR_PATH, imageName) => {
  return (await stat(path.join(ASSETS_DIR_PATH, imageName))).size;
}

export const deleteFile = async (ASSETS_DIR_PATH, imageName) => {
  await unlink(path.join(ASSETS_DIR_PATH, imageName));
}
