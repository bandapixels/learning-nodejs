import {
  Controller,
  Post,
  Req,
  Headers,
  Logger,
  Header,
  Get,
  Param,
  StreamableFile,
  NotFoundException, Res
} from '@nestjs/common';
import { addImageNotify } from 'src/notifications';
import { ImagesService } from 'src/images/images.service';
import { Request, Response } from 'express';
import { PassThrough } from 'node:stream';
import { CacheService } from 'src/cache/cache.service';
import { contentType } from 'mime-types';

@Controller('image')
export class ImageController {
  private readonly logger = new Logger(ImageController.name);

  constructor(
    private imagesService: ImagesService,
    private cacheService: CacheService,
  ) {}

  @Header('Content-Type', 'text/plain')
  @Post()
  async createFile(@Req() req: Request, @Headers('content-type') contentType: string) {
    const data = await this.imagesService.createFile(req, contentType);

    addImageNotify(data);

    this.logger.debug(`Successfully created file: ${data}`);
    this.logger.warn(`New file was created`);

    return data;
  }

  @Header('Content-Disposition', 'inline')
  @Get(':imageName')
  async getFile(
    @Param('imageName') imageName: string,
    @Res() res: Response
  ) {
    const file = await this.cacheService.getFile(imageName);

    if (file) {
      this.logger.warn('Cache HIT!');

      res.setHeader('Content-Type', contentType(imageName));
      return res.send(file);
    }

    try {
      // const fileSize = await getFileSize(this.configService.get<string>('ASSETS_DIR_PATH'), imageName);
      const fileReadStream = await this.imagesService.getFileReadStream(imageName);
      // res.setHeader('Content-Type', contentType(imageName));

      const passThrough = new PassThrough();
      passThrough.on('data', chunk => {
        this.cacheService.appendFileData(`file:${imageName}`, chunk);
      });

      passThrough.on('end', () => {
        this.cacheService.setKeyExpiration(`file:${imageName}`, 60);
      });

      return fileReadStream.pipe(passThrough).pipe(res);
    } catch (e) {
      e.filename = imageName;
      throw new NotFoundException('Requested file not found');
    }
  }
}
