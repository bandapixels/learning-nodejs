import { Module } from '@nestjs/common';
import { ImagesController } from './images.controller';
import { ImageController } from './image/image.controller';
import { ImagesService } from './images.service';
import { CacheModule } from 'src/cache/cache.module';

@Module({
  imports: [CacheModule],
  controllers: [ImagesController, ImageController],
  providers: [ImagesService]
})
export class ImagesModule {}
