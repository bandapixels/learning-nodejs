import { Controller, Get, Render } from '@nestjs/common';
import { listFiles } from 'src/fs';
import { ConfigService } from '@nestjs/config';

@Controller('images')
export class ImagesController {
  constructor(private configService: ConfigService) {}

  @Get()
  @Render('list')
  async listImages() {
    return { images: await listFiles(this.configService.get<string>('ASSETS_DIR_PATH')) };
  }
}
