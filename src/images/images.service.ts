import { Injectable } from '@nestjs/common';
import { randomUUID } from 'node:crypto';
import { open } from 'node:fs/promises';
import { join } from 'node:path';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';
import { extension } from 'mime-types';
import { ReadStream } from 'fs';

@Injectable()
export class ImagesService {
  constructor(private configService: ConfigService) {}

   async createFile(req: Request, contentType: string): Promise<string> {
    const fileName = `${randomUUID()}.${extension(contentType)}`;
    const file = await open(join(this.configService.get<string>('ASSETS_DIR_PATH'), fileName), 'a');
    req.pipe(file.createWriteStream());

    return fileName;
  }

  async getFileReadStream(imageName: string): Promise<ReadStream> {
      return (await open(join(this.configService.get<string>('ASSETS_DIR_PATH'), imageName), 'r')).createReadStream();
  }
}
