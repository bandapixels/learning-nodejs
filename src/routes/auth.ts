import { Router } from 'express';
import { addUser } from '../dataservice';
import * as passport from 'passport';

export const authRouter = Router();

authRouter.get('/me', (req, res) => res.json(req.user));

authRouter.get('/auth/google',
  passport.authenticate('google', { scope: ['profile', 'email'] }));

authRouter.get('/auth/google/callback',
  passport.authenticate('google', { failureRedirect: '/login' }),
  (req, res) => {
    res.redirect('/');
  });

authRouter.post('/signup', async (req, res, next) => {
  const { username, password } = req.body;

  await addUser({ username, password });

  res.sendStatus(204);
});

authRouter.post('/login', passport.authenticate('local'), async (req, res, next) => {
  res.json(req.user);
});
