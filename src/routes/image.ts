import { Router } from 'express';
import { createFile, deleteFile, getFileReadStream, getFileSize } from '../fs';
import { contentType } from 'mime-types';
import { addImageNotify, removeImageNotify } from '../notifications';
import { PassThrough } from 'node:stream';
import Redis from 'ioredis';

const redisClient = new Redis();

export const imageRouter = (ASSETS_DIR_PATH, logger) => {
  const router = Router();

  router
    .route('/:imageName')
    .delete(async (req, res, next) => {
      const imageName = req.params.imageName;
      try {
        await deleteFile(ASSETS_DIR_PATH, imageName);

        removeImageNotify(imageName);

        res.statusCode = 204;
        res.end();
      } catch (e) {
        next(e);
      }
    });

  return router;
}
