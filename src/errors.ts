export const errorHandlingMiddleware = (error, req, res, next) => {
  console.error(error);
  if (error.statusCode) {
    res.statusCode = error.statusCode;
  } else {
    res.statusCode = 404;
  }
  res.render('404', { filename: error.filename ? error.filename : 'File' });
}
