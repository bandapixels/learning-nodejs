import { prepare } from 'src/setup';

export default async () => ({
  ASSETS_DIR_PATH: await prepare(),
});
