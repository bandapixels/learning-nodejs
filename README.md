### Day 1:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AACfqbM2eIGWMID9C6b7aJHda/1_2022-10-03%20at%2017.00.28_2.mp4?dl=0)

- What is NodeJS
- Basics of NodeJS: Buffer, Net, Globals, Crypto, ESM vs CJM
- Simple TCP server
- Manual implementation of Websocket server

Docs:
- [NodeJS documentation](https://nodejs.org/dist/latest/docs/api/)
- [Websocket protocol spec](https://www.rfc-editor.org/rfc/rfc6455)

Homework:
Learn about NodeJS streams by completing tasks in https://github.com/workshopper/stream-adventure
___
### Day 2:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AADue7RC74-KGgxExlO2GL3fa/2_2022-10-05%2016-59-33.mp4?dl=0)

- Basic HTTP server
- Reading request body
- Writing binary from request body to filesystem
- Piping and transforming binary from request body to response

Docs: 
- [NodeJS documentation on HTTP module](https://nodejs.org/dist/latest/docs/api/http.html)
- [HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP)

Homework:

Create HTTP server with next endpoints:
- POST /image - should save request body as a file with random name, but extension should match `Content-Type` header. Should return 201 code and filename of saved file.
- GET /image/{imageName} - return file with correct headers `Content-Type` and `Content-Disposition: attachment; filename="real_filename_from_fs"`
- DELETE /image/{imageName} - delete image and return response with 204 code and without body
- GET /images - return JSON list of existing filenames
- WS /subscribe - websocket connection, all clients should be notified about adding and deleting images to server.
---
### Day 3:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AABRzBy4a69bSnTJW4WxZYyEa/3_2022-10-07%2016-59-54.mp4?dl=0)

- Testing with Jest
- Refactor server code to simplify testing

Docs: 
- [Jest - JavaScript Testing Framework](https://jestjs.io/docs/getting-started)
- [Axios - Promise based HTTP client for the browser and node.js](https://github.com/axios/axios)

Homework:
- Extend endpoint `GET /image/{imageName}` with optional query params `height` and `width` for image resizing (use sharp lib)
- Cover all functionality with tests !!!

---
### Day 4:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AAACRla7zM_e9YQtiRF2Q9_3a/4_2022-10-10%2017-00-32.mp4?dl=0)

- More testing
- Serving static files
- Api-doc with swagger

Docs: 
- [Swagger](https://swagger.io/docs/specification/about/)

Homework:
- Complete integration tests
- Describe all API endpoints in swagger

---
### Day 5:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AACRHlYtwQUKED7QG60i4-KXa/5_2022-10-12%2017-00-43.mp4?dl=0)

- Project structuring
- dotEnv
- Logging with winston and morgan
- Horizontal scaling with PM2

Docs: 
- [DotEnv](https://www.npmjs.com/package/dotenv)
- [Morgan](https://www.npmjs.com/package/morgan)
- [Winston](https://github.com/winstonjs/winston)
- [PM2](https://pm2.keymetrics.io/docs/usage/quick-start/)

---
### Day 6:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AABQycDEp41BiYVRDQ9hAx0ua/6_2022-10-24%2017-03-09.mp4?dl=0)

- Express
  - Middlewares
  - Routing
  - Error handling
- Handlebars

Docs: 
- [Express](https://expressjs.com/)
- [Handlebars](https://handlebarsjs.com/)
- [Hbs](https://www.npmjs.com/package/hbs)

---
### Day 7:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AAAFIug1drauHW-tz2wBdcrXa/7_2022-10-26%2017-02-19.mp4?dl=0)

- Redis
  - Pub/Sub
  - Caching
  - Incrementing
  - Session storage
- Express sessions

Docs: 
- [Redis](https://redis.io/)
- [ioredis](https://handlebarsjs.com/)
- [Express-session](https://www.npmjs.com/package/express-session)
- [connect-redis](https://www.npmjs.com/package/connect-redis)

---
### Day 8:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AACIF_41f4LANL6Zh4HwURMHa/8_2022-10-31%2017-15-22.mp4?dl=0)

- Authentication/Authorization
- SQL basics

Docs: 
- [PassportJS](https://www.passportjs.org/)
- [Sqlite3](https://www.sqlite.org/index.html)
- [Sqlite3-NPM](https://www.npmjs.com/package/sqlite3)

---
### Day 9:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AADdxgmJjpIorMDiI76dYzYCa/9_2022-11-02%2017-00-23.mp4?dl=0)

- Auth with Google (OAuth2)
- More SQL basics

Docs: 
- [passport-google-oauth2](https://github.com/jaredhanson/passport-google-oauth2)
- [Sqlite3 - FOREIGN KEYS](https://www.sqlite.org/foreignkeys.html)
- [Sql JOIN](https://www.w3schools.com/sql/sql_join.asp)

---
### Day 10:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AAB9kYLe8ANVL4egy4Ap6_rda?dl=0&preview=10_2022-11-04+17-01-12.mp4)

- ORM sequelize.org

Docs: 
- [https://sequelize.org/](https://sequelize.org/)

Homework:
- Create new db model for files with next columns:
  - filename: string
  - uploader: int - foreign key to User
- Create new db model for permissions with next columns:
  - type: string  (one of 'list', 'read', 'delete')
  - user: int - foreign key to User
  - file: int - foreign key to File
- [Create relation Many to Many between Users and Files through Permissions](https://sequelize.org/docs/v6/advanced-association-concepts/advanced-many-to-many/)

---
### Day 11:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AAB9kYLe8ANVL4egy4Ap6_rda?dl=0&preview=11_2022-11-07+16-59-56.mp4)

- NestJS
- Easy migration from ExpressJS to NestJS 

Docs: 
- [NestJS](https://docs.nestjs.com/)

---
### Day 12:

### [Screen recording](https://www.dropbox.com/sh/zx2peybapblwj3b/AAB9kYLe8ANVL4egy4Ap6_rda?dl=0&preview=12_2022-11-09+17-01-09.mp4)

- More NestJS

Docs: 
- [NestJS](https://docs.nestjs.com/)

---
### Day 13:

### [Screen recording]()

- MongoDB

Docs: 
- [MongoDB](https://www.mongodb.com/docs/)
- [Mongoose](https://mongoosejs.com/docs/guide.html)

---

# [FINAL TASK](https://docs.google.com/document/d/1nTK-cifppDzjQabZT8bXnn0nUKUeRdTFQLPyzwGCllY/edit?usp=sharing)
