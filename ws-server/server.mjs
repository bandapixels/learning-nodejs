import { createServer } from 'node:net';
import { createHash } from 'node:crypto';

String.prototype.prepareMask = function () {
  return parseInt(this.split(' ').join(''), 2);
};

const UUID = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';

const PORT = 3000 || process.env.PORT;

const FIN_MASK =
   '1 0 0 0 0 0 0 0'.prepareMask();

const OPCODE_MASK =
   '0 0 0 0 1 1 1 1'.prepareMask();

const MASK_MASK =
                   '1 0 0 0 0 0 0 0'.prepareMask();

const MASK_PAYLOAD_LENGTH =
                   '0 1 1 1 1 1 1 1'.prepareMask();

//  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
// +-+-+-+-+-------+-+-------------+-------------------------------+
// |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
// |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
// |N|V|V|V|       |S|             |   (if payload len==126/127)   |
// | |1|2|3|       |K|             |                               |
// +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
// |     Extended payload length continued, if payload len == 127  |
// + - - - - - - - - - - - - - - - +-------------------------------+
// |                               |Masking-key, if MASK set to 1  |
// +-------------------------------+-------------------------------+
// | Masking-key (continued)       |          Payload Data         |
// +-------------------------------- - - - - - - - - - - - - - - - +
// :                     Payload Data continued ...                :
// + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
// |                     Payload Data continued ...                |
// +---------------------------------------------------------------+

createServer()
  .on('connection', (socket) => {
    let isHandshakeCompleted = false;
    let dataArray = [];
    socket.on(
      'data',
      data => {
        if (isHandshakeCompleted) {
          const [firstByte, secondByte, mask1, mask2, mask3, mask4, ...payload] = data;
          const mask = [mask1, mask2, mask3, mask4];
          const FIN = (firstByte & FIN_MASK) >> 7;

          const OPCODE = firstByte & OPCODE_MASK;

          const MASK = (secondByte & MASK_MASK) >> 7;
          const PAYLOAD_LENGTH = secondByte & MASK_PAYLOAD_LENGTH;

          console.log({FIN, OPCODE, MASK, PAYLOAD_LENGTH});

          const demaskedPayload = payload.map((v, i) => {
            return v ^ mask[i % 4];
          });

          const textPayload = Buffer.from(demaskedPayload).toString('utf8');
          dataArray.push(textPayload);

          if (FIN === 1) {
            emit('result', dataArray.join(''));
          }

          dataArray.length = 0;

          console.log(`Data from client: ${textPayload}`);

          return;
        }
        const stringData = data.toString('utf8');
        const [requestParams, ...headers] = stringData.split('\n');
        const headersObj = headers.reduce((res, row) => {
          if (!row.length) {
            return res;
          }
          const [key, value] = row.split(':');
          res[key.trim()] = value ? value.trim() : '';
          return res;
        }, {});

        const secWebSocketKeyHeader = headersObj['Sec-WebSocket-Key'];
        const valueToHash = secWebSocketKeyHeader + UUID;
        const hash = createHash('sha1');
        hash.update(valueToHash);
        const secWebSocketAccept = hash.digest('base64');
        socket.write(`HTTP/1.1 101 Switching Protocols\r
Upgrade: websocket\r
Connection: Upgrade\r
Sec-WebSocket-Accept: ${secWebSocketAccept}\r
\r
`);
        isHandshakeCompleted = true;
      }
    );
  })
  .on('error', err => console.error(err))
  .listen(PORT, () => console.log(`Server listening on port ${PORT}`));
// localhost:3000
