'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GoogleProfile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      GoogleProfile.belongsTo(models.User);
    }
  }
  GoogleProfile.init({
    sub: DataTypes.STRING,
    name: DataTypes.STRING,
    given_name: DataTypes.STRING,
    family_name: DataTypes.STRING,
    picture: DataTypes.STRING,
    email: DataTypes.STRING,
    email_verified: DataTypes.BOOLEAN,
    locale: DataTypes.STRING,
    UserId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'GoogleProfile',
  });
  return GoogleProfile;
};
