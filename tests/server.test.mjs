import { createServer } from '../src/controller.mjs';
import { join } from 'node:path'
import { mkdtemp, readdir, rm } from 'node:fs/promises'
import { tmpdir } from 'node:os';
import axios from 'axios';
import { emptyDir } from 'fs-extra';

const UUID_REGEX = /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/;

describe('Integration tests', () => {
  let server;
  let dir;

  beforeAll(async () => {
    dir = await mkdtemp(join(tmpdir(), 'images-'));
    server = await createServer(dir);
    await new Promise((resolve) => {
      server.once('listening', resolve);
      axios.defaults.baseURL = `http://localhost:${server.address().port}`;
    });
  });

  afterEach(async () => {
    await emptyDir(dir);
  })

  afterAll(async () => {
    server.close();
    await rm(dir, { recursive: true, force: true });
  });

  describe('POST /image', () => {
    test('save file with correct name and extension', async () => {
      await axios.post('/image', Buffer.alloc(1024), {
        headers: {
          'Content-Type': 'image/jpeg'
        }
      });

      const [file] = await readdir(dir);
      const [fileName, fileExtension] = file.split('.');

      expect(fileName).toMatch(UUID_REGEX);
      expect(fileExtension).toBe('jpeg');
    });

    test('response contains correct code and filename', async () => {
      const res = await axios.post('/image', Buffer.alloc(1024), {
        headers: {
          'Content-Type': 'image/jpeg'
        }
      });

      const [file] = await readdir(dir);

      expect(res.data).toBe(file);
      expect(res.status).toBe(201);
    });
  });
});
